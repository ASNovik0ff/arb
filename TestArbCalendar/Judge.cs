﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestArbCalendar
{
    class Judge
    {
        public List<string> allJudge = new List<string>();
        public Judge()
        {
            Regex stringProxy = new Regex("((.){36})");
            foreach (Match m in stringProxy.Matches(File.ReadAllText(@"G:\TestArbCalendar\TestArbCalendar\Judes.txt")))
            {
                allJudge.Add(m.Groups[1].Value);
            }
        }
        public string getListJudges(int count)
        {
            string judges = string.Empty;
            for (int i = 0; i < count; i++)
            {
                if (i < count - 1)
                {
                    
                    judges += "\"" + allJudge[i] + "\",";
                }
                else
                {
                    judges += "\"" + allJudge[i] + "\"";
                }     
            }
            return judges;
        }
        public void DeleteJudges(int count)
        {
            for (int i = 0; i < count; i++)
            {
                allJudge.RemoveAt(0);
            }       
        }
    }
}
