﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestArbCalendar
{
    class ListProxy
    {
        public List<MyProxy> allProxy = new List<MyProxy>();
        public Stack<MyProxy> allProxyStack = new Stack<MyProxy>();
        List<MyProxy> BlackList = new List<MyProxy>();
        List<MyProxy> WhiteList = new List<MyProxy>();

        public int startCountProxy, finCoubntProxy;
        public int countRequest;
        public ListProxy()
        {
            Regex stringProxy = new Regex("((\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+))\\s*\\:?\\s*(\\d{2,6})");
            foreach (Match m in stringProxy.Matches(File.ReadAllText(@"G:\TestArbCalendar\1234.txt")))
            {
                allProxy.Add(new MyProxy(m.Groups[1].Value, Convert.ToInt32(m.Groups[6].Value)));
                allProxyStack.Push(new MyProxy(m.Groups[1].Value, Convert.ToInt32(m.Groups[6].Value)));
            }
            /* foreach (Match m in stringProxy.Matches(File.ReadAllText(@"D:\ListProxy.txt")))
             {
                 allProxy.Add(new MyProxy(m.Groups[1].Value, Convert.ToInt32(m.Groups[6].Value)));
                 allProxyStack.Push(new MyProxy(m.Groups[1].Value, Convert.ToInt32(m.Groups[6].Value)));
             }*/
            startCountProxy = finCoubntProxy = allProxy.Count;
            countRequest = 0;
        }
        public void saveListProxy()
        {

            File.WriteAllText(@"D:\AllLIstProxy.txt", "");
            File.WriteAllText(@"D:\BlackLIstProxy.txt", "");
            File.WriteAllText(@"D:\WhiteLIstProxy.txt", "");
            foreach (MyProxy item in allProxy)
            {
                File.AppendAllText(@"D:\AllLIstProxy.txt", item.hostString + ":" + item.port + "\r\n");
            }
            foreach (MyProxy item in BlackList)
            {
                File.AppendAllText(@"D:\BlackLIstProxy.txt", item.hostString + ":" + item.port + "\r\n");
            }
            foreach (MyProxy item in WhiteList)
            {
                File.AppendAllText(@"D:\WhiteLIstProxy.txt", item.hostString + ":" + item.port + "\r\n");
            }
        }
        public MyProxyAndIndex GetProxy() // return random proxy from list Proxy
        {
            int index = new Random().Next() % (allProxy.Count - 1);

            return new MyProxyAndIndex(allProxy[index],index);
        }
        public MyProxy GetProxy_() // return random proxy from list Proxy
        {
            int index = new Random().Next() % (allProxy.Count - 1);
            return allProxyStack.Pop();
        }
        public void deleteProxyFromList(int index)
        {
            allProxy.RemoveAt(index);
            finCoubntProxy--;
        }
        public void AddProxyToList(int item, MyProxy myProxy)
        {
            if (item == 0)
            {
                BlackList.Add(myProxy);
            }
            else
            {
                WhiteList.Add(myProxy);
            }
            
        }

    }
    class MyProxy
    {
        public string hostString;
        public int port;
        public MyProxy(string hostString, int port)
        {
            this.hostString = hostString;
            this.port = port;
        }
    }
    class MyProxyAndIndex
    {
        public MyProxy myProxy;
        public int index;
        public MyProxyAndIndex(MyProxy myProxy, int index)
        {
            this.myProxy = myProxy;
            this.index = index;
        }
    }
}
