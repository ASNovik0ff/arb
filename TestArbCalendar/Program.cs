﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestArbCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            CookieContainer cookie = new CookieContainer();
            ListProxy listProxy = new ListProxy();
            Judge judges = new Judge();
            Regex caseNumber = new Regex("\"CaseId\":\"(.+?)\"");
            Regex errorMessageCatch = new Regex("try again");
            string path = "Cases.txt";
            int countDeleteJudges = 0;
            int startCountJudges;
            DateTime startTime = DateTime.Now;
            File.WriteAllText(path, "");

            startCountJudges = 250;
            do
            {
                if (judges.allJudge.Count > 250 && startCountJudges > 250)
                {
                    startCountJudges = 250;
                }
                else if (judges.allJudge.Count > 250 && startCountJudges <= 250)
                {

                }
                else
                {
                    startCountJudges = judges.allJudge.Count;
                }

                bool succsess = false;
                string findCase = string.Empty;
                do
                {
                    succsess = true;
                    string data = "{\"needConfirm\":false,\"DateFrom\":\"2018-08-14T00:00:00\",\"Sides\":[],\"Cases\":[],\"Judges\":["
                    + judges.getListJudges(startCountJudges) + "],\"JudgesEx\":[],\"Courts\":[]}";
                    
                    findCase = PostRequestSearch("http://rad.arbitr.ru/Rad/TimetableDay", data, cookie, listProxy);
                    Console.Write(startCountJudges.ToString() + "\tRequest\t" + listProxy.countRequest + "\n");

                    foreach (Match m in errorMessageCatch.Matches(findCase))
                    {
                        Console.Write("false:\t" + startCountJudges.ToString() + "\n");
                        if (startCountJudges > 20)
                        {
                            startCountJudges /= 2;
                            startCountJudges++;
                            succsess = false;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(3000);
                            Console.Write("Pause 3000\n");
                            startCountJudges /= 2;
                            startCountJudges++;
                            if (startCountJudges == 1) // удаляем 1 судью и выводим ее в файл
                            {
                                File.AppendAllText("failjudges.txt", judges.getListJudges(startCountJudges) + "\r\n");
                                judges.DeleteJudges(startCountJudges);
                                countDeleteJudges += startCountJudges;
                            }
                        }
                       
                    }

                } while (!succsess);

                bool tata = false;
                foreach (Match m in caseNumber.Matches(findCase))
                {
                    File.AppendAllText(path, m.Groups[1].Value + "\n");
                    tata = true;
                }
                if (tata)
                {
                    judges.DeleteJudges(startCountJudges);
                    countDeleteJudges += startCountJudges;
                    File.WriteAllText(judges.allJudge.Count.ToString() + ".txt", findCase);
                    Console.Write("success:\t" + startCountJudges.ToString() + "\n");
                    Console.Write("Осталось проксей:\t" + listProxy.finCoubntProxy.ToString() + "\t Осталось судей:\t" + judges.allJudge.Count.ToString() + "\n");
                    startCountJudges *= 2;
                    startCountJudges++;
                }

              

            } while (judges.allJudge.Count > 0);


            Console.Write(listProxy.finCoubntProxy.ToString() + "\t" + countDeleteJudges + "\t" + listProxy.countRequest);

            File.AppendAllText(path, "Количество запросов:" + listProxy.countRequest + "\n");
            File.AppendAllText(path, "Время скачивания - " + DateTime.Now.Subtract(startTime));


            //listProxy.saveListProxy();

            Console.Read();
        }
        public static string PostRequestSearch(string mainLink, string data, CookieContainer cookie, ListProxy listProxy)
        {
            int tryCount = 10;
            int tryIndex = 0;
            bool error = true;
            string page = "";
            MyProxyAndIndex myProxyAndIndex = listProxy.GetProxy();
            do
            {
                tryIndex++;
                try
                {
                    listProxy.countRequest++;
                    string postData = data;
                    HttpWebRequest req = WebRequest.Create(mainLink) as HttpWebRequest;
                    req.Proxy = new WebProxy(myProxyAndIndex.myProxy.hostString, myProxyAndIndex.myProxy.port);
                    Console.Write(myProxyAndIndex.myProxy.hostString + ": " + myProxyAndIndex.myProxy.port.ToString() + "\n");
                    req.Method = "POST";
                    req.Headers.Add("Accept-Language", "en-US,en;q=0.9");
                    req.Accept = "application/json, text/javascript, */*";
                    req.ContentType = "application/json";
                    req.Host = "rad.arbitr.ru";
                    req.Timeout = 30000;
                    req.CookieContainer = cookie;
                    req.Headers.Add("X-Requested-With", "XMLHttpRequest");
                    req.Referer = "http://rad.arbitr.ru/";
                    req.Headers.Add("Origin", "http://rad.arbitr.ru");
                    req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36 OPR/54.0.2952.71";
                    req.Headers.Add("x-date-format", "iso");
                    byte[] byteArray = Encoding.GetEncoding("utf-8").GetBytes(postData);
                    req.ContentLength = byteArray.Length;
                    Stream dataStream = req.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
                    {
                        cookie.Add(resp.Cookies);
                        using (Stream s = resp.GetResponseStream())
                        using (StreamReader sr = new StreamReader(s))
                            page = sr.ReadToEnd();
                    }
                    Regex BadRequest = new Regex("Bad Request");
                    Regex errorMessage = new Regex("\"Внутренняя ошибка сервера\"");
                    Regex html_ = new Regex("<(.+?)>");
                    Regex timing_ = new Regex("{\"Result\":{\"mode\":null,\"monthSwitcher\":false,\"needConfirm\":true,\"month\":0,\"year\":0,\"day\":0");
                    foreach (Match m in errorMessage.Matches(page))
                    {
                        page = "try again";
                        listProxy.AddProxyToList(0, listProxy.allProxy[myProxyAndIndex.index]);
                        listProxy.deleteProxyFromList(myProxyAndIndex.index);
                        myProxyAndIndex = listProxy.GetProxy();
                        error = true;
                    }
                    foreach (Match m in BadRequest.Matches(page))
                    {
                        page = "try again";
                        listProxy.AddProxyToList(0, listProxy.allProxy[myProxyAndIndex.index]);
                        listProxy.deleteProxyFromList(myProxyAndIndex.index);
                        myProxyAndIndex = listProxy.GetProxy();
                        error = true;
                    }
                    foreach (Match m in timing_.Matches(page))
                    {
                        page = "try again";
                        listProxy.AddProxyToList(0, listProxy.allProxy[myProxyAndIndex.index]);
                        listProxy.deleteProxyFromList(myProxyAndIndex.index);
                        myProxyAndIndex = listProxy.GetProxy();
                        error = true;
                    }
                    foreach (Match m in html_.Matches(page))
                    {
                        page = "try again";
                        listProxy.AddProxyToList(0, listProxy.allProxy[myProxyAndIndex.index]);
                        listProxy.deleteProxyFromList(myProxyAndIndex.index);
                        myProxyAndIndex = listProxy.GetProxy();
                        error = true;
                    }
                    error = false;
                }
                catch
                {
                    listProxy.AddProxyToList(0, listProxy.allProxy[myProxyAndIndex.index]);
                    listProxy.deleteProxyFromList(myProxyAndIndex.index);
                    myProxyAndIndex = listProxy.GetProxy();
                    error = true;
                }
            } while (error && tryIndex < tryCount);

            if (tryIndex > 9)
            {
                page = "try again";
            }

            listProxy.AddProxyToList(1, listProxy.allProxy[myProxyAndIndex.index]);
            listProxy.deleteProxyFromList(myProxyAndIndex.index);

            return page;
        }
    }
}
